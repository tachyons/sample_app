# Preview all emails at http://localhost:3000/rails/mailers/employer
class EmployerMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/employer/email_verification
  def email_verification
    EmployerMailer.email_verification
  end
end
