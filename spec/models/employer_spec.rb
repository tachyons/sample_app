# == Schema Information
#
# Table name: employers
#
#  id                              :integer          not null, primary key
#  name                            :string
#  email                           :string
#  date_of_birth                   :date
#  gender                          :string
#  location                        :string
#  phone_number                    :string
#  email_verified                  :boolean          default(FALSE)
#  phone_number_verified           :boolean          default(FALSE)
#  email_verification_token        :string
#  phone_number_verification_token :string
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#

require 'rails_helper'

RSpec.describe Employer, type: :model do
  let(:employer_params) { { name: 'Andy', email: 'sample@sample.com', location: 'Bangalore', phone_number: '123456789' } }

  it 'Phone is not verified by default' do
    employer = Employer.create!(employer_params)

    expect(employer.phone_number_verified?).to eq(false)
  end
  it 'Email is not verified by default' do
    employer = Employer.create!(employer_params)

    expect(employer.email_verified?).to eq(false)
  end

  it 'Update email verification token' do
    employer = Employer.create!(employer_params)
    token = employer.email_verification_token
    employer.update_attributes(email: 'another@another.com')
    expect(employer.email_verification_token).not_to eq(token)
  end
  it 'Email verification token un altered when phone number changes' do
    employer = Employer.create!(employer_params)
    token = employer.email_verification_token
    employer.update_attributes(phone_number: '987654321')
    expect(employer.email_verification_token).to eq(token)
  end
  it 'Able to verify email using correct token' do
    employer = Employer.create!(employer_params)
    token = employer.email_verification_token
    expect(employer.verify_email(token)).to eq(true)
  end
  it 'Should not be able to verify email twice' do
    employer = Employer.create!(employer_params)
    token = employer.email_verification_token
    employer.verify_email(token)
    expect(employer.verify_email(token)).to eq(false)
  end
end
