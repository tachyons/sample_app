== README

# Requirements

Ruby 2.2
TWILIO account
mailgun account

# Installation

cd project_path
export TWILIO_SID=''
export TWILIO_NUMBER=''
export TWILIO_TOKEN=''

export MAILGUN_KEY=''
export MAILGUN_DOMAIN=''
bundle install
rails s
