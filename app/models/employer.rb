# == Schema Information
#
# Table name: employers
#
#  id                              :integer          not null, primary key
#  name                            :string
#  email                           :string
#  date_of_birth                   :date
#  gender                          :string
#  location                        :string
#  phone_number                    :string
#  email_verified                  :boolean          default(FALSE)
#  phone_number_verified           :boolean          default(FALSE)
#  email_verification_token        :string
#  phone_number_verification_token :string
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#

class Employer < ActiveRecord::Base
  CONFIRMATION_TOKEN_LENGTH = 5
  validates_presence_of :email, :location, :phone_number, :name
  before_update :reset_token
  after_create :reset_email_token, :reset_phone_number_token
  include Rails.application.routes.url_helpers

  def verified?
    email_verified? & phone_number_verified?
  end

  def verify_phone_number(token)
    if phone_number_verified?
      errors.add(:base, 'Already Verified')
      return false
    end
    if phone_number_verification_token == token
      update_attributes(phone_number_verified: true)
    else
      errors.add(:base, 'Wrong validation code')
      false
    end
  end

  def verify_email(token)
    if email_verified?
      errors.add(:base, 'Already Verified')
      return false
    end
    if email_verification_token == token
      update_attributes(email_verified: true)
    else
      errors.add(:base, 'Wrong validation code')
      false
    end
  end

  private

  def reset_token
    reset_phone_number_token if phone_number_changed?
    reset_email_token if email_changed?
  end

  def reset_phone_number_token
    transaction do
      self.phone_number_verification_token = generate_confirmation_token
      self.phone_number_verified = false
    end
    send_phone_number_verification_token
  end

  def reset_email_token
    transaction do
      self.email_verification_token = generate_confirmation_token
      self.email_verified = false
    end
    send_email_verification_token
  end

  def generate_confirmation_token
    array = Array(0..9) + Array('a'..'z') + Array('A'..'Z')
    array.sample(CONFIRMATION_TOKEN_LENGTH).join
  end

  def send_phone_number_verification_token
    twilio_client.messages.create(
      from: ENV['TWILIO_NUMBER'],
      to: phone_number,
      body: "Your verification code  \n#{phone_number_verification_token}, click  #{verify_attribute_employer_url(self,code: phone_number_verification_token,type: :phone_number )}"
    ).delay
  end

  def send_email_verification_token
    EmployerMailer.email_verification(self).deliver_later
  end

  def twilio_client
    @twilio_client ||= Twilio::REST::Client.new ENV['TWILIO_SID'], ENV['TWILIO_TOKEN']
  end
end
