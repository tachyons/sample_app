class EmployerMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.employer.email_verification.subject
  #
  def email_verification(employer)
    @greeting = 'Hi'
    @employer = employer
    mail(to: "#{employer.name} <#{employer.email}>", subject: 'Email Confirmation')
  end
end
