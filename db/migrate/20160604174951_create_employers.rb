class CreateEmployers < ActiveRecord::Migration
  def change
    create_table :employers do |t|
      t.string :name
      t.string :email
      t.date :date_of_birth
      t.string :gender
      t.string :location
      t.string :phone_number
      t.boolean :email_verified, default: false
      t.boolean :phone_number_verified, default: false
      t.string :email_verification_token
      t.string :phone_number_verification_token

      t.timestamps null: false
    end
  end
end
